import { SortingAnimation } from "../../models/SortingAnimation"

export const generateSortingAnimation = (
  currIdx: number, 
  compIdx: number, 
  sortedIndc: number[], 
  arrState: number[],
  secCompIdx?: number,
): SortingAnimation => {

  return {
    currentIdx: currIdx, 
    compareIdx: compIdx, 
    secondaryCompareIdx: secCompIdx,  
    currentArrayState: arrState,
    sortedIndices: sortedIndc, 
  }
}