import { SortingAnimation } from "../models/SortingAnimation";
import { generateSortingAnimation } from "./helpers/generateSortingAnimation";

export const getQuickSortAnimations = (arrState: number[]) => {
  const arr = [ ...arrState ]; 
  let animations: SortingAnimation[] = [];
  let sortedIndc: number[] = arrState.map((_, idx) => idx); 
  quickSort(arr, 0, arr.length - 1, animations); 
  // Final animation to show everything is sorted.
  sortedIndc.forEach(idx => {
    const sorted = Array.from(Array(idx+1).keys());
    animations.push(generateSortingAnimation(-1, -1, sorted, [ ...arr ], -1));
  }); 
  return animations; 
}

const quickSort = (arr: number[], start: number = 0, end: number = arr.length - 1, animations: SortingAnimation[]) => {
  if(start >= end) return; 
  // Get the pivot idx 
  const pivotIdx = pivotHelper(arr, start, end, animations); 
  // Run quick sort on the left of the pivot. 
  quickSort(arr, start, pivotIdx - 1, animations); 
  // Run quick sort on the right of the pivot. 
  quickSort(arr, pivotIdx + 1, end, animations); 
  return arr; 
}

const pivotHelper = (arr: number[], start: number, end: number, animations: SortingAnimation[]): number => {
  let pivot = arr[start]; 
  let swapIdx = start; 

  const swap = (arr: number[], swapIdx1: number, swapIdx2: number) => {
    let temp = arr[swapIdx1]; 
    arr[swapIdx1] = arr[swapIdx2]; 
    arr[swapIdx2] = temp; 
    // Generate an animation whenever we make a swap. 
    animations.push(generateSortingAnimation(start, swapIdx2, [], [ ...arr ], swapIdx1));
  }

  for(let i = start + 1; i <= end; i++) {
    // Generate an animation to highlight the comparison elements
    animations.push(generateSortingAnimation(start, i, [], [ ...arr ], swapIdx));

    // Check of the current is smaller than the pivot. 
    if(arr[i] < pivot) {
      swapIdx++; 
      // Swap
      swap(arr, swapIdx, i); 
    }
  }
  // Swap the swapIdx with the current start. 
  swap(arr, swapIdx, start); 
  return swapIdx; 
}