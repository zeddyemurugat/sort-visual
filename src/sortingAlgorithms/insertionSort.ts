import { SortingAnimation } from "../models/SortingAnimation";
import { generateSortingAnimation } from "./helpers/generateSortingAnimation";

export const getInsertionSortAnimations = (arrState: number[]) => {
  let arr = [ ...arrState ]; 
  let animations: SortingAnimation[] = [];
  let sortedIndc: number[] = []; 

  for(let i = 1; i < arr.length; i++) {
    let curr = arr[i]; 
    let j = i - 1; 
    while(arr[j] > curr) {
      // Generate animation for comparing i with j each time j changes
      animations.push(generateSortingAnimation(i, j, [], [ ...arr ], -1));
      arr[j + 1] = arr[j];
      arr[j] = curr;
      // Generate animation for each time the arr changes
      animations.push(generateSortingAnimation(i, j + 1, [], [ ...arr ], j)); 
      j--; 
    }
    arr[j + 1] = curr; 
    sortedIndc.push(i - 1); 
  }
  // Generate completion animation at the end. 
  const latestAnimation = animations[animations.length - 1]; 
  latestAnimation.sortedIndices = [ ...sortedIndc, arr.length - 1 ]; 
  animations.push(generateSortingAnimation(-1, -1, [ ...sortedIndc ], [ ...arr ], -1)); 
  return animations; 
}