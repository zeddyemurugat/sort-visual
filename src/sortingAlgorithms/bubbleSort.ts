import { SortingAnimation } from "../models/SortingAnimation";
import { generateSortingAnimation } from "./helpers/generateSortingAnimation";

export const getBubbleSortAnimations = (arrState: number[]) => {
  let arr = [ ...arrState ]; 
  let animations: SortingAnimation[] = [];
  let sortedIndc: number[] = []; 

  for(let i = arr.length; i >= 0; i--) {
    for(let j = 0; j < i; j++) {
      // Generate an animation state before comparison. 
      animations.push(generateSortingAnimation(j, j+1, [ ...sortedIndc ], [ ...arr ]));

      if(arr[j] > arr[j + 1]) {
        let temp = arr[j]; 
        arr[j] = arr[j + 1]; 
        arr[j + 1] = temp; 
        // Generate an animation for the swap
        animations.push(generateSortingAnimation(j, j+1, [...sortedIndc], [ ...arr ])); 
      }
    }
    sortedIndc.push(i - 1); 
    // Generate an animation for current sorted Indices
    const latestAnimation = animations[animations.length - 1]; 
    latestAnimation.sortedIndices = [ ...sortedIndc ]; 
    animations.push(latestAnimation); 
  }

  return animations; 
}