import { SortingAnimation } from "../models/SortingAnimation";
import { generateSortingAnimation } from "./helpers/generateSortingAnimation";

export const getSelectionSortAnimations = (arrState: number[]) => {
  let arr = [ ...arrState ]; 
  let animations: SortingAnimation[] = [];
  let sortedIndc: number[] = []; 

  for(let i = 0; i < arr.length; i++) {
    let min = i; 
    for(let j = i + 1; j < arr.length; j++) {
      // Generate an animation for comparing the current min and the current val we are looking at.
      animations.push(generateSortingAnimation(i, j, [ ...sortedIndc ], [ ...arr ], min)); 
      if(arr[j] < arr[min]) {
        // Generate animation for when we update the min 
        min = j
        animations.push(generateSortingAnimation(i, j, [ ...sortedIndc ], [ ...arr ], min)); 
      }; 
    }
    // Swap 
    let temp = arr[i]; 
    arr[i] = arr[min]; 
    arr[min] = temp; 

    // Generate animation for when we swap and update the current i as sorted. 
    sortedIndc.push(i); 
    const latestAnimation = animations[animations.length - 1]; 
    latestAnimation.sortedIndices = [ ...sortedIndc ]; 
    animations.push(latestAnimation); 
  }
  return animations; 
}