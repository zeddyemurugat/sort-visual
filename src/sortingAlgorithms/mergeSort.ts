import { SortingAnimation } from "../models/SortingAnimation";
import { generateSortingAnimation } from "./helpers/generateSortingAnimation";

export const getMergeSortAnimations = (arrState: number[]) => {
  let arr = [ ...arrState ]; 
  let animations: SortingAnimation[] = [];
  let sortedIndc: number[] = arr.map((_, idx) => idx); 
  mergeSort(arr, 0, arr.length - 1, animations); 
  // Final animation to show everything is sorted.
  sortedIndc.forEach(idx => {
    const sorted = Array.from(Array(idx+1).keys());
    animations.push(generateSortingAnimation(-1, -1, sorted, [ ...arr ]));
  }); 
  return animations; 
}

const mergeSort = (arr: number[], start: number = 0, end: number = arr.length - 1, animations: SortingAnimation[]) => {
  if(start >= end) return; 
  const middle = Math.floor((start + end) / 2); 
  // Split the left side into a sorted arr.
  mergeSort(arr, start, middle, animations); 
  // Split the right side into a sorted arr. 
  mergeSort(arr, middle + 1, end, animations); 
  // Merge the two sorted arrays back together.
  doMerge(arr, start, middle, end, animations);  
}

const doMerge = (arr: number[], start: number, middle: number, end: number, animations: SortingAnimation[]) => {
  let lenLeftSub = middle - start + 1; 
  let lenRightSub = end - middle; 
  // Init auxiliary arrays - For overwriting indices. 
  let leftSub = [], rightSub = []; 
  // Loop and fill the aux arrays with the actual values from the main array. 
  for(let i = 0; i < lenLeftSub; i++) {
    leftSub[i] = arr[start + i]; 
  }
  for(let j = 0; j < lenRightSub; j++) {
    rightSub[j] = arr[middle + 1 + j]; 
  }
  // Initialize variables to track the index at which we are at for the different arrays. 
  let i = 0; 
  let j = 0; 
  let k = start; 

  while(i < lenLeftSub && j < lenRightSub) {
    animations.push(generateSortingAnimation(k, (middle + 1 + j), [], [ ...arr ]));
    if(leftSub[i] <= rightSub[j]) {
      arr[k] = leftSub[i]; 
      i++; 
      animations.push(generateSortingAnimation(k, (middle + 1 + j), [], [ ...arr ]));
    }
    else {
      arr[k] = rightSub[j]; 
      j++; 
      animations.push(generateSortingAnimation(k, (middle + 1 + j), [], [ ...arr ]));
    }
    k++; 
  }
  while(i < lenLeftSub) {
    arr[k] = leftSub[i]; 
    i++;
    k++; 
    animations.push(generateSortingAnimation(k, (middle + 1 + j), [], [ ...arr ]));
  }
  while(j < lenRightSub) {
    arr[k] = rightSub[j]; 
    j++; 
    k++; 
    animations.push(generateSortingAnimation(k, (middle + 1 + j), [], [ ...arr ]));
  }
}