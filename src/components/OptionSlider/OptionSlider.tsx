import React from 'react';
import Box from '@mui/material/Box';
import Stack from '@mui/material/Stack';
import Slider from '@mui/material/Slider';
import './OptionSlider.css'; 

interface SliderOpts {
  value: number; 
  label: string; 
  isSorting: boolean; 
  handleChange: (event: Event, newValue: number | number[]) => void; 
  min: number; 
  max: number; 
}

export const OptionSlider:React.FC<SliderOpts> = (props) => {
  const { value, label, isSorting, handleChange, min, max } = props; 

  return (
    <div className="option-slider">
      <div className="label">{ label }</div>
      <div className="slider">
        <Box sx={{ width: 180 }}>
          <Stack spacing={2} direction="row" sx={{ mb: 1 }} alignItems="center">
            <Slider aria-label="Volume" 
              disabled={ isSorting }
              min={min} 
              max={max} 
              value={value} 
              onChange={handleChange} 
            />
          </Stack>
        </Box>
      </div>
    </div>
  )
}