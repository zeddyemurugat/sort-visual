import React from 'react';

import './SideBar.css'; 
import logo from '../../assets/sort-logo.png'; 
import { OptionSelectAlgorithm } from '../OptionSelectAlgorithm/OptionSelectAlgorithm';
import { OptionSlider } from '../OptionSlider/OptionSlider';
import { SideBarOpts } from '../../models/SideBarOpts';
import { ARRAY_MAX, ARRAY_MIN, SORTING_SPEED_MAX, SORTING_SPEED_MIN } from '../../constants';

interface SideBarProps {  
  sideBarOpts: SideBarOpts; 
}

export const SideBar:React.FC<SideBarProps> = (props) => {
  const { 
    isSorting, 
    algoChangeOpts, 
    arrSizeChangeOpts, 
    sortingSpeedChangeOpts 
  } = props.sideBarOpts;

  return (
    <div className="side-bar">
      <img src={logo} alt="Logo" className="logo" />
      <div className="heading">Sort<strong>Visual</strong></div>
      <div className="subheading">Where sorting algorithms come to life.</div>
      <OptionSelectAlgorithm algoChangeOpts={ algoChangeOpts } />

      <div className="divider"></div>

      <OptionSlider 
        isSorting={ isSorting }
        value={ arrSizeChangeOpts.arrSize } 
        handleChange={ arrSizeChangeOpts.handleArrSizeChange } 
        label="Size of Array" 
        min={ ARRAY_MIN }
        max={ ARRAY_MAX }
      />
      <OptionSlider 
        isSorting={ isSorting }
        value={ sortingSpeedChangeOpts.sortingSpeed } 
        handleChange={ sortingSpeedChangeOpts.handleSortingSpeedChange } 
        label="Sorting Speed" 
        min={ SORTING_SPEED_MIN }
        max={ SORTING_SPEED_MAX }
      />
    </div>
  )
}