import React from "react";
import './Visualizer.css'; 
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import ShuffleOnIcon from '@mui/icons-material/ShuffleOn';
import SortIcon from '@mui/icons-material/Sort';

import { generateRandomArraySize } from "../../App/generateRandomArraySize";
import { ARRAY_MAX, ARRAY_MIN, SORTING_SPEED_MAX, SORTING_SPEED_MIN } from "../../constants";

import { getBubbleSortAnimations } from "../../sortingAlgorithms/bubbleSort";
import { getSelectionSortAnimations } from "../../sortingAlgorithms/selectionSort";
import { getInsertionSortAnimations } from "../../sortingAlgorithms/insertionSort";
import { getMergeSortAnimations } from "../../sortingAlgorithms/mergeSort";
import { getQuickSortAnimations } from "../../sortingAlgorithms/quickSort";

interface VisualizerProps {
  algo: "bubble" | "insertion" | "selection" | "merge" | "quick"; 
  isSorting: boolean; 
  sortingSpeed: number; 
  randomArray: number[];
  refreshRandomArray: (newValue: number) => void;
  setIsSorting:       (newValue: boolean) => void;
}

export const Visualizer:React.FC<VisualizerProps> = (props) => {
  const { 
    algo, 
    isSorting, 
    sortingSpeed, 
    randomArray, 
    refreshRandomArray, 
    setIsSorting 
  } = props; 

  const [ arrayState, updateArrayState ] = React.useState(randomArray);
  const [ currentIdx, setCurrentIdx ] = React.useState<number>(-1); 
  const [ compareIdx, setCompareIdx ] = React.useState<number>(-1); 
  const [ secondaryIdx, setSecondaryCompareIdx ] = React.useState<number>(-1); 
  const [ sortedIndices, setSortedIndices ] = React.useState<number[]>([]);
  const [ timeouts, trackTimeouts ] = React.useState<any[]>([]); 
  const calculatedSortSpeed = Math.max(SORTING_SPEED_MIN, SORTING_SPEED_MAX - sortingSpeed);

  const getSortingAnimation = (arr: number[], sortAlgo: "bubble" | "insertion" | "selection" | "merge" | "quick") => {
    const sort = {
      "bubble":    (arr: number[]) => getBubbleSortAnimations(arr), 
      "insertion": (arr: number[]) => getInsertionSortAnimations(arr),
      "selection": (arr: number[]) => getSelectionSortAnimations(arr),
      "merge":     (arr: number[]) => getMergeSortAnimations(arr),
      "quick":     (arr: number[]) => getQuickSortAnimations(arr)
    }
    return sort[sortAlgo](arr); 
  }

  const sort = () => {
    setIsSorting(true);
    const animations = getSortingAnimation(arrayState, algo); 
    const timeouts: any[] = []; 
    animations.forEach((animation, idx) => {
      timeouts.push(setTimeout(() => {
        const { currentIdx, compareIdx, currentArrayState, sortedIndices, secondaryCompareIdx } = animation; 
        setCurrentIdx(currentIdx); 
        setCompareIdx(compareIdx);
        if(secondaryCompareIdx) setSecondaryCompareIdx(secondaryCompareIdx); 
        setSortedIndices((arr) => [ ...arr, ...sortedIndices ]); 
        updateArrayState(currentArrayState); 
      }, (idx * calculatedSortSpeed))); 
    }); 
    trackTimeouts(timeouts);
  }

  // const runBubbleSort = () => {
  //   setIsSorting(true); 
  //   const animations = getBubbleSortAnimations(arrayState); 
  //   const timeouts: any[] = []; 
  //   animations.forEach((animation, idx) => {
  //     timeouts.push(setTimeout(() => {
  //       const { currentIdx, compareIdx, currentArrayState, sortedIndices } = animation; 
  //       setCurrentIdx(currentIdx); 
  //       setCompareIdx(compareIdx); 
  //       setSortedIndices((arr) => [ ...arr, ...sortedIndices ]); 
  //       updateArrayState(currentArrayState); 
  //     }, (idx * calculatedSortSpeed))); 
  //   }); 
  //   trackTimeouts(timeouts); 
  // }

  // const runSelectionSort = () => {
  //   setIsSorting(true); 
  //   const animations = getSelectionSortAnimations(arrayState); 
  //   const timeouts: any[] = []; 
  //   animations.forEach((animation, idx) => {
  //     timeouts.push(setTimeout(() => {
  //       const { currentIdx, compareIdx, secondaryCompareIdx, currentArrayState, sortedIndices } = animation; 
  //       setCurrentIdx(currentIdx); 
  //       setCompareIdx(compareIdx); 
  //       setSecondaryCompareIdx(secondaryCompareIdx as number); 
  //       setSortedIndices((arr) => [ ...arr, ...sortedIndices ]); 
  //       updateArrayState(currentArrayState); 
  //     }, (idx * calculatedSortSpeed))); 
  //   }); 
  //   trackTimeouts(timeouts); 
  // }

  // const runInsertionSort = () => {
  //   setIsSorting(true); 
  //   const animations = getInsertionSortAnimations(arrayState); 
  //   const timeouts: any[] = []; 
  //   animations.forEach((animation, idx) => {
  //     timeouts.push(setTimeout(() => {
  //       const { currentIdx, compareIdx, secondaryCompareIdx, currentArrayState, sortedIndices } = animation; 
  //       setCurrentIdx(currentIdx); 
  //       setCompareIdx(compareIdx); 
  //       setSecondaryCompareIdx(secondaryCompareIdx as number); 
  //       setSortedIndices((arr) => [ ...arr, ...sortedIndices ]); 
  //       updateArrayState(currentArrayState); 
  //     }, (idx * calculatedSortSpeed))); 
  //   }); 
  //   trackTimeouts(timeouts); 
  // }

  // const runMergeSort = () => {
  //   setIsSorting(true); 
  //   const animations = getMergeSortAnimations(arrayState); 
  //   const timeouts: any[] = []; 
  //   animations.forEach((animation, idx) => {
  //     timeouts.push(setTimeout(() => {
  //       const { currentIdx, compareIdx, currentArrayState, sortedIndices } = animation; 
  //       setCurrentIdx(currentIdx); 
  //       setCompareIdx(compareIdx); 
  //       setSortedIndices((arr) => [ ...arr, ...sortedIndices ]); 
  //       updateArrayState(currentArrayState); 
  //     }, (idx * calculatedSortSpeed))); 
  //   }); 
  //   trackTimeouts(timeouts); 
  // }

  // const runQuickSort = () => {
  //   setIsSorting(true); 
  //   const animations = getQuickSortAnimations(arrayState); 
  //   // console.log('FN:runQuickSort', animations); 
  //   const timeouts: any[] = []; 
  //   animations.forEach((animation, idx) => {
  //     timeouts.push(setTimeout(() => {
  //       const { currentIdx, compareIdx, secondaryCompareIdx, currentArrayState, sortedIndices } = animation; 
  //       setCurrentIdx(currentIdx); 
  //       setCompareIdx(compareIdx); 
  //       setSecondaryCompareIdx(secondaryCompareIdx as number); 
  //       setSortedIndices((arr) => [ ...arr, ...sortedIndices ]); 
  //       updateArrayState(currentArrayState); 
  //     }, (idx * calculatedSortSpeed))); 
  //   }); 
  //   trackTimeouts(timeouts); 
  // }
   
  // If the input sizes changes, reset the state. 
  if(arrayState.length && arrayState.length !== randomArray.length) {
    setCurrentIdx(-1); 
    setCompareIdx(-1); 
    setSecondaryCompareIdx(-1); 
    setSortedIndices([]); 
    updateArrayState(randomArray); 
    timeouts.forEach(timer => clearTimeout(timer)); 
  }

  const getBarBackgroundColor = (idx: number) => {
    if(sortedIndices.length && sortedIndices.includes(idx as number)) 
      return '#71efa3'; 
    else if(idx === currentIdx)
      return '#fdd2bf'; 
    else if(idx === compareIdx) 
      return '#ffee93'; 
    else if(idx === secondaryIdx)
      return '#F79F78'; 
    else 
      return '#E6E6E6'; 
  }

  const refreshArray = () => {
    let randomSize = generateRandomArraySize(ARRAY_MIN, ARRAY_MAX); 
    // Recalculate random size until it isn't equal with current array size. 
    while(randomSize === randomArray.length) 
      randomSize = generateRandomArraySize(ARRAY_MIN, ARRAY_MAX); 

    refreshRandomArray(randomSize); 
  }

  const showSortBtn = !isSorting && sortedIndices.length === 0; 
  return (
    <div>
      <Stack spacing={1} direction="row" className="center-items">
        <Button variant="contained" className="btn shuffle-btn" onClick={ () => refreshArray() }><ShuffleOnIcon /> <span>New Random Array</span></Button>
        { showSortBtn ? <Button variant="contained" className="btn sort-btn" onClick={ () => sort() }><SortIcon /> <span>Sort Array</span></Button> 
                      : null
        }
      </Stack>
      <div className="visualizer">
        <div className="label">{ algo[0].toUpperCase() + algo.slice(1) } Sort</div>
        <div className="bars-cont" style={{ height: 350 }}>
          { arrayState.map((arr, key) => <div key={key} className="bar" style={{ height: arr, backgroundColor: getBarBackgroundColor(key) }}></div>) }
        </div>
      </div>
    </div>
  )
}