import React from 'react';
import './OptionSelectAlgorithm.css'; 
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select'; 
import { AlgoChangeOpts } from '../../models/AlgoChangeOpts';

interface OptionSelectAlgorithmProps { 
  algoChangeOpts: AlgoChangeOpts; 
}

export const OptionSelectAlgorithm:React.FC<OptionSelectAlgorithmProps> = (props) => {
  const { algo, handleAlgoChange } = props.algoChangeOpts; 

  return (
    <div className="option-select-algo">
      <Box sx={{ minWidth: 120 }}>
        <FormControl fullWidth>
          <InputLabel>Select Algorithm</InputLabel>
          <Select
            value={algo}
            label="Select Algorithm"
            onChange={handleAlgoChange}
          >
            <MenuItem value='bubble'>Bubble Sort</MenuItem>
            <MenuItem value='selection'>Selection Sort</MenuItem>
            <MenuItem value='insertion'>Insertion Sort</MenuItem>
            <MenuItem value='merge'>Merge Sort</MenuItem>
            <MenuItem value='quick'>Quick Sort</MenuItem>
          </Select>
        </FormControl>
      </Box>
    </div>
  )
}