import { AlgoChangeOpts } from "./AlgoChangeOpts";
import { ArrSizeChangeOpts } from "./ArrSizeChangeOpts";
import { SortingSpeedChangeOpts } from "./SortingSpeedChangeOpts";

export interface SideBarOpts {
  isSorting: boolean; 
  algoChangeOpts: AlgoChangeOpts;
  arrSizeChangeOpts: ArrSizeChangeOpts;  
  sortingSpeedChangeOpts: SortingSpeedChangeOpts; 
}