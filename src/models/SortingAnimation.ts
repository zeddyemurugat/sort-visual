export interface SortingAnimation {
  currentIdx: number; 
  compareIdx: number; 
  currentArrayState: number[]; 
  sortedIndices: number[]; 

  secondaryCompareIdx?: number; 
}