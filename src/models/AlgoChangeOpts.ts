import { SelectChangeEvent } from "@mui/material";

export interface AlgoChangeOpts {
  algo: "bubble" | "selection" | "insertion" | "merge" | "quick"; 
  handleAlgoChange: (event: SelectChangeEvent) => void; 
}