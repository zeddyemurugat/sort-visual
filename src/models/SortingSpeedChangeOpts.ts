export interface SortingSpeedChangeOpts {
  sortingSpeed: number
  handleSortingSpeedChange: (event: Event, newValue: number | number[]) => void; 
}