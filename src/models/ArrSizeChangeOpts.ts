export interface ArrSizeChangeOpts {
  arrSize: number
  handleArrSizeChange: (event: Event, newValue: number | number[]) => void; 
}