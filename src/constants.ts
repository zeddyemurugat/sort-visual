export const ARRAY_MIN = 10;
export const ARRAY_MAX = 25; 

export const SORTING_SPEED_MAX = 200; 
export const SORTING_SPEED_MIN = 15; 