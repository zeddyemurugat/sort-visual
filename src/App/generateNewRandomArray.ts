export const generateNewRandomArray = (arraySize: number) => {
  let barSizes = []; 
  for(let i = 0; i < arraySize; i++) {
    barSizes.push(Math.floor(Math.random() * 300) + 100); 
  }
  return barSizes;
}