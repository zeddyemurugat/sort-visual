import { SelectChangeEvent } from '@mui/material';
import React from 'react';

import './App.css';
import { SideBar } from '../components/SideBar/SideBar';
import { SideBarOpts } from '../models/SideBarOpts';
import { generateNewRandomArray } from './generateNewRandomArray';
import { generateRandomArraySize } from './generateRandomArraySize';
import { ARRAY_MAX, ARRAY_MIN } from '../constants';
import { Visualizer } from '../components/Visualizer/Visualizer';

declare type AlgoTypes = 'bubble' | 'selection' | 'insertion' | 'merge' | 'quick'; 

function App() { 
  const [algo, setAlgo]                  = React.useState<AlgoTypes>("bubble");
  const [arrSize, setArrSize]            = React.useState(10);
  const [randomArray, setNewRandomArray] = React.useState<number[]>(generateNewRandomArray(10)); 
  const [sortingSpeed, setSortingSpeed]  = React.useState(15);
  const [isSorting, setIsSorting]        = React.useState(false); 

  const refreshRandomArray = (size: number | number[]) => {
    setArrSize(size as number); 
    setNewRandomArray(generateNewRandomArray(size as number));
  }

  const resetState = (arrSize: number) => {
    setIsSorting(false); 
    refreshRandomArray(arrSize); 
  }

  const handleArrSizeChange = (event: any, newValue: number | number[]) => {
    setArrSize(newValue as number); 
    resetState(newValue as number); 
  }
  
  const handleAlgoChange = (event: SelectChangeEvent) => {
    setAlgo(event.target.value as AlgoTypes);
    resetState(generateRandomArraySize(ARRAY_MIN, ARRAY_MAX)); 
  }

  const handleSortingSpeedChange = (event: Event, newValue: number | number[]) => {
    setSortingSpeed(newValue as number); 
  }

  const sideBarOpts: SideBarOpts = {
    isSorting,
    algoChangeOpts:         { algo, handleAlgoChange },
    arrSizeChangeOpts:      { arrSize, handleArrSizeChange },
    sortingSpeedChangeOpts: { sortingSpeed, handleSortingSpeedChange }
  }

  return (
    <div className="App">
      <div className="container">
        <SideBar sideBarOpts={ sideBarOpts } />

        <div className="content">
          <Visualizer 
            algo={ algo } 
            isSorting={ isSorting }
            sortingSpeed={ sortingSpeed } 
            randomArray={ randomArray } 
            refreshRandomArray={ resetState }
            setIsSorting={ setIsSorting }
          /> 
        </div>
      </div>
    </div>
  );
}

export default App;
